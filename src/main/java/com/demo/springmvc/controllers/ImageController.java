/**
 * 
 */
package com.demo.springmvc.controllers;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author root
 *
 */
@Controller
public class ImageController {
	@RequestMapping(value = "/image/preview"  , method = RequestMethod.GET)
	public String preview(Model model , HttpServletRequest request, HttpServletResponse response) {
//		HttpSession session = request.getSession();
//		
//		if (session.getAttribute("username") != null) {
//			return "image";
//		} else {
//			model.addAttribute("message", "You are need login first");
//			return "index";
//		}
		if(checkCookie(request)) {
			return "image";
		}else {
			model.addAttribute("message", "You are need login first"); 
			return "index";
		}
	}
	
	private Boolean checkCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		Boolean logIn =  false;
		for(Cookie cookie : cookies) {
			if("username".equals(cookie.getName()) && 
					cookie.getValue() != null) {
				System.out.println("hihihihi");
				logIn =  true;
			} 
		}
		return logIn;
	}
	
	@RequestMapping(value = "/image/update"  , method = RequestMethod.POST)
	public String update(Model model ,
			@RequestParam("file") MultipartFile multipartFile , 
			HttpServletRequest request, HttpServletResponse response) {
		String directory = "/resources/images";
		File file = new File(directory, multipartFile.getOriginalFilename());
		try {
			FileUtils.writeByteArrayToFile(file, multipartFile.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "image";
	}
}
