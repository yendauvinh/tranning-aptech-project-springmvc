/**
 * 
 */
package com.demo.springmvc.controllers;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.springmvc.dao.UserDAO;
import com.demo.springmvc.entity.User;
import com.demo.springmvc.services.UserService;

/**
 * @author root
 *
 */
@Controller
public class LoginController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserDAO userDao;
	
	@RequestMapping(value = "" , method = RequestMethod.GET)
	public String index() {
		return "index";
	}
	
	@RequestMapping(value = "/login" , method = RequestMethod.POST)
	public String login(Model model , @ModelAttribute User user , HttpServletRequest request, HttpServletResponse response) {
		if(userService.login(user.getUsername(), user.getPassword())) {
			model.addAttribute("username", user.getUsername());
			//save use session.
			HttpSession session =  request.getSession();
			session.setAttribute("username", user.getUsername());
			Cookie userNameCookie = createCookie("username", user.getUsername());
			response.addCookie(userNameCookie);
			//return "welcome";
			List<User> users = userDao.getAll();
			model.addAttribute("users", users);
			return "userList";
		}else {
			model.addAttribute("message", "You are logged in as ");
			return "index";
		}
	}
	
	@RequestMapping(value = "/create" , method = RequestMethod.GET)
	public String createGet(Model model , @ModelAttribute User user){
		return "newuser";
	}
	
	@RequestMapping(value = "/edit" , method = RequestMethod.GET)
	public String edit(Model model , 
			@PathParam("id") String id, 
			HttpServletRequest request, HttpServletResponse response){
		int userId = Integer.parseInt(id);
		User currentUser = userDao.getById(userId);
		model.addAttribute("user", currentUser);
		return "newuser";
	}
	
	@RequestMapping(value = "/delete" , method = RequestMethod.GET)
	public String delete(Model model , 
			@PathParam("id") String id, 
			HttpServletRequest request, HttpServletResponse response){
		int userId = Integer.parseInt(id);
		User currentUser = userDao.getById(userId);
		userDao.remove(currentUser);
		List<User> users = userDao.getAll();
		model.addAttribute("users", users);
		return "userList";
	}
	
	@RequestMapping(value = "/create" , method = RequestMethod.POST)
	public String create(Model model , @ModelAttribute User user){
		if(user.getId() != null){
			userDao.update(user);
		}else{
			userDao.save(user);
		}
		
		List<User> users = userDao.getAll();
		model.addAttribute("users", users);
		return "userList";
	}
	
	private Cookie createCookie(String cookieName, String cookieValue) {
		Cookie cookie = new Cookie(cookieName, cookieValue);
		// Set expiry date after 24 Hrs for both the cookies.
		cookie.setMaxAge(60*60*24);
		return cookie;
	}
}
