/**
 * 
 */
package com.demo.springmvc.services;

/**
 * @author root
 *
 */
public interface UserService {
	public Boolean login(String username, String password);
}
