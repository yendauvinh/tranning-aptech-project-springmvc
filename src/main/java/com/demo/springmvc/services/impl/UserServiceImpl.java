/**
 * 
 */
package com.demo.springmvc.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.springmvc.dao.UserDAO;
import com.demo.springmvc.services.UserService;

/**
 * @author root
 *
 */
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDAO userDao;
	
	public Boolean login(String username, String password) {
		Boolean isLogin = userDao.login(username, password);
		return isLogin;
	}
}
