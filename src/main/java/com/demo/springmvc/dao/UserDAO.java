/**
 * 
 */
package com.demo.springmvc.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.demo.springmvc.entity.User;

/**
 * @author YenDauVinh
 *
 */
@Repository
@Transactional
public class UserDAO {
	@Autowired
	private SessionFactory sessionFactory;

	
	public Boolean login(String username, String password) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("FROM User WHERE username = :username AND password = :password");
		query.setParameter("username", username);
		query.setParameter("password", password);
		List<User> users = query.getResultList();
		if(users != null && !users.isEmpty()) {
			return true;
		}else {
			return false;
		}
	}
	
	public List<User> getAll(){
		Query query =  sessionFactory.getCurrentSession().createQuery("FROM User");
		List<User> users = query.getResultList();
		return users;
	}
	
	//insert
	public User save(User user){
		sessionFactory.getCurrentSession().save(user);
		return user;
	}
	
	public User update(User user){
		sessionFactory.getCurrentSession().update(user);
		return user;
	}
	
	//delete
	public void remove(User user){
		sessionFactory.getCurrentSession().remove(user);
	}
	
	public User getById(Integer id){
		Query query = sessionFactory.getCurrentSession().createQuery("FROM User WHERE id = :id");
		query.setParameter("id", id);
		List<User> users = query.getResultList();
		if(users.isEmpty()){
			 return null;
		}else{
			return users.get(0);
		}
	}
	
}
