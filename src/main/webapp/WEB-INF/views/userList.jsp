<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>APTECH</title>
</head>
<body>
	<a href="${pageContext.request.contextPath}/create">CREATE</a>
	<h1>USERS</h1>
	<table class="container">
		<tr>
			<th>Id</th>
			<th>UserName</th>
			<th>Password</th>
			<th>Role</th>
			<th>Age</th>
			<th>Actions</th>
		</tr>
		<c:forEach items="${users}" var="u">
			<tr>
				<td>${u.id}</td>
				<td>${u.username}</td>
				<td>${u.password}</td>
				<td>${u.role}</td>
				<td>${u.age}</td>
				<td>
					<a href="${pageContext.request.contextPath}/edit?id=${u.id}">EDIT</a>
					<a href="${pageContext.request.contextPath}/delete?id=${u.id}">DELETE</a>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>