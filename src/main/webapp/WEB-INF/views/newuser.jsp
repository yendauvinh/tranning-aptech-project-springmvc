<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NEW</title>
</head>
<body>
	<h1>NEW USER</h1>
	<form action='<spring:url value="/create"/>' method="post" >
		<table>
			<tr>
				<td>Id</td>
				<td><input type="text" readonly="readonly" name="id" value="${user.id}"></td>
			</tr>
			<tr>
				<td>Username</td>
				<td><input type="text" name="username" value="${user.username}"></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="password" name="password" value="${user.password}"></td>
			</tr>
			<tr>
				<td>Role</td>
				<td><input type="text" name="role" value="${user.role}"></td>
			</tr>
			<tr>
				<td>Age</td>
				<td><input type="text" name="age" value="${user.age}"></td>
			</tr>
			<tr>
				<td><button type="submit">Submit</button></td>
			</tr>
		</table>
	</form>
</body>
</html>