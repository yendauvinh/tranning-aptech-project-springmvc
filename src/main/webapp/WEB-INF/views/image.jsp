<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>APTECH</title>
</head>
<body>
	<h2>UPDATE PROFILES</h2>
	<img src="https://www.w3schools.com/html/pic_trulli.jpg" alt="Trulli"
		width="500" height="333">
	<form action='<spring:url value="/image/update"/>' method="post"
		enctype="multipart/form-data">
		<table>
			<tr>
				<td><form:label path="file">Select a file to upload</form:label></td>
				<td><input type="file" name="file" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form>
</body>
</html>